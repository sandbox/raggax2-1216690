<?php 
/**
* @file
* Administration functions / callbacks for Unfuddle Reports
*/

function unfuddle_reports_admin() {
  drupal_add_css(drupal_get_path('module', 'unfuddle_reports') . '/unfuddle_reports.css');
  $rtn = '<div id="uf_admin"><div class="uf_actions"><fieldset>';
  $rtn .= '<legend>' . t('Manual Update') . '</legend>';
  $rtn .= l(t('Update Users'), 'unfuddle/update/users');
  $rtn .= l(t('Update Projects'), 'unfuddle/update/projects');
  $rtn .= l(t('Update Tickets'), 'unfuddle/update/tickets');
  $rtn .= l(t('Update Hours'), 'unfuddle/update/hours');
  $rtn .= '</fieldset></div>';            
  
  $rtn .= '</div><!-- #uf_admin -->';
  return $rtn;
}