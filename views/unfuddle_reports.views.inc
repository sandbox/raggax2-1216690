<?php
  
/**
* @file
* Provides views data for unfuddle_reports.module
*/
function unfuddle_reports_views_data() { 
  /* User Table */
  $data['uf_users']['table']['group'] = t('Unfuddle Reports');
  $data['uf_users']['table']['base'] = array(
    'field' => 'ufid',
    'title' => t('Unfuddle Users'),
    'help' => t('Provides a link to users in the Unfuddle system'),
  );
  
  $data['uf_users']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    )
  );
  
  $data['uf_users']['uid'] = array( 
    'title' => t('UserID'),
    'help' => t('The Drupal UserID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,  
    ),   
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'relationship' => array(
       'base' => 'users',
       'handler' => 'views_handler_relationship',
       'label' => 'Unfuddle - Users',
    ),
  );
   
  $data['uf_users']['ufid'] = array( 
   'title' => t('Unfuddle UserID - Users'),
   'help' => t('The Unfuddle UserID'),
   'field' => array(
     'handler' => 'views_handler_field',
     'click sortable' => TRUE,  
   ), 
   'relationship' => array(
      'base' => 'users',
      'handler' => 'views_handler_relationship',
      'label' => 'Unfuddle - Users',
   ),
   'sort' => array(
      'handler' => 'views_handler_sort',      
    ),
  );  
  
  $data['uf_users']['username'] = array(
    'title' => t('Unfuddle Username'),
    'help' => t('The username as imported from Unfuddle'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,  
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',      
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),    
  );
  
  /* Hours Table */
  $data['uf_hours']['table']['group'] = t('Unfuddle Reports');
  $data['uf_hours']['table']['join'] = array(
    'uf_users' => array(
      'left_field' => 'ufid',
      'field' => 'ufid',
    ), 
    'users' => array(
      'left_field' => 'ufid', 
      'left_table' => 'uf_users',
      'field' => 'ufid',
    ),
    'uf_tickets' => array(
      'left_field' => 'uftid',
      'field' => 'uftid',
    ),
  );
    
  $data['uf_hours']['description'] = array(
    'title' => t('Hours - Description'),
    'help' => t('The description for the hours line item'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,  
    ),
  ); 
  
  $data['uf_hours']['hours'] = array(
    'title' => t('Hours - Number of Hours'),
    'help' => t('The number of hours reported'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),  
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',      
    ),
  );

  $data['uf_hours']['timestamp'] = array(
    'title' => t('Unfuddle Hours Timestamp'),
    'help' => t('The Unfuddle UserID in the Hours table'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,  
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_date',
      'allow empty' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',      
    ),  
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );   
  
  /* Tickets Table */ 
  $data['uf_tickets']['table']['group'] = t('Unfuddle Reports');
  $data['uf_tickets']['table']['join'] = array(
    'uf_project' => array(
      'left_field' => 'ufpid',
      'field' => 'ufpid',
    ),
    'uf_hours' => array(
      'left_field' => 'uftid',
      'field' => 'uftid',
    ),
  );   
  
  $data['uf_tickets']['title'] = array(
    'title' => t('Tickets - Title'),
    'help' => t('The title of the referencing ticket'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,  
    ),    
  );
  
  $data['uf_tickets']['status'] = array( 
    'title' => t('Tickets - Status'),
    'help' => t('The status of the referencing ticket'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,  
    ),     
  );
  
  /* Projects Table */  
  $data['uf_project']['table']['group'] = t('Unfuddle Reports');
  $data['uf_project']['title'] = array(
    'title' => t('Project - Title'),
    'help' => t('Title of an Unfuddle Project'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,  
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    )
  );
  return $data;
}